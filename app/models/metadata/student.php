<?php
namespace app\models\metadata;
/**
*Meta Class of Student model
*having @param $id $firstname $lastname $contact $address
*$pk - a primary key $tablename and $dbfields
*/
class student {
  public $id = "";
  public $firstname = "";
  public $lastname = "";
  public $contact = "";
  public $address = "";

  public $pk = "id";
  public $tablename = "student";
  public $dbfields = ["firstname", "lastname", "contact", "address" ];
  
}

?>
