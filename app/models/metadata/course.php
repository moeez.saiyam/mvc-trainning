<?php
namespace app\models\metadata;
/**
*Meta Class of Course model
*having @param $id $name $credithours $pk - a primary key
*$tablename and $dbfields
*/
class course {
  public $id = "";
  public $name = "";
  public $credithours = "";

  public $pk = "id";
  public $tablename = "course";
  public $dbfields = ["name", "credithours" ];

}

?>
