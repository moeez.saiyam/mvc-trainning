<?php namespace app\views\student\edit ?>
<!DOCTYPE html>
<html lang="en"
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <title>EDIT Student</title>
</head>
<body>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="?controller=home&action=show">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Student</li>
    </ol>
  </nav>
  <h1>EDIT Student</h1>
  <div class="form">
    <form method="post" >
      <input type="hidden" name="controller" value="student"><br>
      <input type="hidden" name="action" value="update"><br>
      ENTER ID YOU WANT TO EDIT:<br>
      <input type="text" name="id" value="" required><br>
      New First name:<br>
      <input type="text" name="firstname" value="" required><br>
      New Last name:<br>
      <input type="text" name="lastname" value="" required><br>
      New Contact:<br>
      <input type="text" name="contact" value="" required><br>
      New Address:<br>
      <input type="text" name="address" value="" required><br><br>
      <input type="submit" value="Submit">
    </form>
  </div>
  <?php if ($param == "SUCCESSFUL!"){ ?>
    <div class="alert" style="width:11%; padding-top: 40px;">
      <div class="alert alert-success">
        <p align="center"><strong><?php print_r($param); ?></strong></p>
      </div>
    </div>
  <?php } else if($param == "FAILED!") { ?>
    <div class="alert" style="width:11%; padding-top: 40px;">
      <div class="alert alert-danger">
        <p align="center"><strong><?php print_r($param); ?></strong></p>
      </div>
    </div>
  <?php } ?>
</body>
</html>
