<?php namespace app\views\student\show ?>

<!DOCTYPE html>
<html lang="en"
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <title>SHOW Student</title>
</head>
<body>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="?controller=home&action=show">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Show Student</li>
    </ol>
  </nav>
  <h1>SHOW Student</h1>
  <input type="hidden" name="controller" value="course"><br>
  <input type="hidden" name="action" value="show"><br>

  <div class="container">


    <div class="row">
      <div class="col-sm-2">
        <p><mark><strong>ID</strong></mark></p>
      </div>
      <div class="col-sm-2">
        <p><mark><strong>First Name</strong></mark></p>
      </div>
      <div class="col-sm-2">
        <p><mark><strong>Last Name</strong></mark></p>
      </div>
      <div class="col-sm-2">
        <p><mark><strong>Contact</strong></mark></p>
      </div>
      <div class="col-sm-2">
        <p><mark><strong>Address</strong></mark></p>
      </div>

    </div>
    <?php if ($param == "Nothing"){ ?>
      <div class="alert">
        <div class="alert alert-danger">
          <p align="center"><strong>NOTHING TO SHOW</strong></p>
        </div>
      </div>
    <?php } else {

      foreach ($param as $key) { ?>

        <div class="row">
          <div class="col-sm-2">
            <p><mark><?php echo $key['id']; ?></mark></p>
          </div>
          <div class="col-sm-2">
            <p><mark><?php echo $key['firstname']; ?></mark></p>
          </div>
          <div class="col-sm-2">
            <p><mark><?php echo $key['lastname']; ?></mark></p>
          </div>
          <div class="col-sm-2">
            <p><mark><?php echo $key['contact']; ?></mark></p>
          </div>
          <div class="col-sm-2">
            <p><mark><?php echo $key['address']; ?></mark></p>
          </div>

        </div>
      <?php } } ?>

    </div>

  </body>
  </html>
