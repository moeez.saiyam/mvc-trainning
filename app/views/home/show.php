<?php namespace app\views\home\show ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title>HOME</title>
  </head>
  <body>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
      </ol>
    </nav>

    <div class="container">
      <h1>MANAGEMENT PORTAL</h1>
     <div class="row">
        <div class="col-sm-12">
          <h3><b>Student</b></h3>
        </div>
      </div>
        <div class="row">
          <div class="col-sm-3">
            <p><mark><a href="?controller=student&action=add">ADD Student</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=student&action=delete">Delete Student</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=student&action=update">Update Student</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=student&action=show">Show Student</a></mark></p>
          </div>
        </div>


      <div class="row">
        <div class="col-sm-12">
          <h3><b>Teacher</b></h3>
        </div>
        <div class="row">
        </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=teacher&action=add">ADD Teacher</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=teacher&action=delete">Delete Teacher</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=teacher&action=update">Update Teacher</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=teacher&action=show">Show Teacher</a></mark></p>
          </div>
        </div>


      <div class="row">
        <div class="col-sm-12">
          <h3><b>Course</b></h3>
        </div>
      </div>
        <div class="row">
          <div class="col-sm-3">
            <p><mark><a href="?controller=course&action=add">ADD Course</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=course&action=delete">Delete Course</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=course&action=update">Update Course</a></mark></p>
          </div>
          <div class="col-sm-3">
            <p><mark><a href="?controller=course&action=show">Show Course</a></mark></p>
          </div>
        </div>


    </div>
  </body>
</html>
