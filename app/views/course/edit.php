<?php namespace app\views\course\edit ?>
<!DOCTYPE html>
<html lang="en"
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <title>EDIT COURSE</title>
</head>
<body>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="?controller=home&action=show">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Course</li>
    </ol>
  </nav>
  <h1>EDIT COURSE</h1>
  <div class="form">
    <form method="post" >
      <input type="hidden" name="controller" value="course"><br>
      <input type="hidden" name="action" value="update"><br>
      ENTER ID YOU WANT TO EDIT:<br>
      <input type="text" name="id" value="" required><br>
      New Course name:<br>
      <input type="text" name="name" value="" required><br>
      New Credit hours:<br>
      <input type="text" name="credithours" value="" required><br>
      <br>
      <input type="submit" value="Submit">
    </form>
  </div>

  <?php if ($param == "SUCCESSFUL!"){ ?>
    <div class="alert" style="width:11%; padding-top: 40px;">
      <div class="alert alert-success">
        <p align="center"><strong><?php print_r($param); ?></strong></p>
      </div>
    </div>
  <?php } else if($param == "FAILED!") { ?>
    <div class="alert" style="width:11%; padding-top: 40px;">
      <div class="alert alert-danger">
        <p align="center"><strong><?php print_r($param); ?></strong></p>
      </div>
    </div>
  <?php } ?>
</body>
</html>
