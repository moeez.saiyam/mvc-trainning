<?php
namespace core\controllers;

use core\controllers\controllerInterface as controllerInterface;
use core\views\viewManager as viewManager;
use system\classes\request as request;
use core\models\modelFactory as modelFactory;

/**
*Base Controller Class
*/
class baseController  implements controllerInterface {

  private $req;
  private $view;
  private $model;
  /**
  *Constructor that initialize request, ViewManager and ModelFactory
  */
  public function __construct()
  {
    $this->req = request::getInstance();
    $this->view = new viewManager;

    if($this->req->getParams() != null || $this->req->getMethod() == 'show')
    {
      $this->model = modelFactory::buildModel($this->req->getController());
    }

  }

  /**
  *ADD function that returns the view page from viewManager
  */
  public function add()
  {
    /**
    *Checks if request has parameters then call model else return view only
    */
    if($this->req->getParams() == null)
    {
      $this->view->view("add");
      return true;
    }
    else {

      $this->model->makeEntity($this->req->getParams());
      $result = $this->model->add();
      if($result)
      {
        $result = "SUCCESSFUL!";
        $this->view->view("add",$result);
        return true;
      }
      else {
        $result = "FAILED!";
        $this->view->view("add",$result);
        return false;
      }
    }
  }

  /**
  *Delete function that returns the view page from viewManager
  */
  public function delete()
  {
    /**
    *Checks if request has parameters then call model else return view only
    */
    if($this->req->getParams() == null)
    {
      $this->view->view("delete");
    }
    else {
      $this->model->makeEntity($this->req->getParams());
      $result = $this->model->delete();
      if($result)
      {
        $result = "SUCCESSFUL!";
        $this->view->view("delete",$result);
        return true;
      }
      else {
        $result = "FAILED!";
        $this->view->view("delete",$result);
        return false;
      }
    }
  }
  /**
  *Update function that returns the view page from viewManager
  */
  public function update()
  {
    /**
    *Checks if request has parameters then call model else return view only
    */
    if($this->req->getParams() == null)
    {
      $this->view->view("edit");
      return true;
    }
    else {
      $this->model->makeEntity($this->req->getParams());
      $result = $this->model->update();
      if($result)
      {
        $result = "SUCCESSFUL!";
        $this->view->view("edit",$result);
        return true;
      }
      else {
        $result = "FAILED!";
        $this->view->view("edit",$result);
        return false;
      }

    }

  }

  /**
  *Show function that returns the view page from viewManager
  */
  public function show()
  {
    /**
    *Checks if model exists for controller else return default show view
    */
    if($this->model)
    {
      $result = $this->model->show();

      if ($result)
      {
        $this->view->view("show",$result);
        return true;
      }
      else {
        $result = "Nothing";
        $this->view->view("show",$result);
        return false;
      }
    }
    else {
      $this->view->view("show");
      return true;
    }

  }

  /**
  *Call the Action/Method of baseClass if exists
  */
  public function callAction()
  {
    $method = $this->req->getMethod();
    if (method_exists($this, $method))
    {
      $this->$method();
      return true;
    }
    else {
      echo "Method Not Found";
      return false;
    }
  }

  /**
  *Setter Function that sets request @param
  */
  public function setreq($req)
  {
    $this->req = $req;
  }

  /**
  *Setter Function that sets View @param
  */

  public function  setview($view)
  {
    $this->view = $view;
  }
  /**
  *Setter Function that sets Model @param
  */
  public function setmodel($model)
  {
    $this->model = $model;
  }

}



?>
