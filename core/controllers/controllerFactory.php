<?php

namespace core\controllers;
/**
*Controller Factory Class that is responsible for building a controller
*/
class controllerFactory {

  public function __construct()
  {
  }
  /**
  *Funtion BuildController is taking a parameter controller and
  *it returns an instance of that @controller if it is the subclass
  *of baseController
  */
  public static function buildController ($controller) {

    $controller = 'app\controllers\\'.$controller."Controller";

    if (is_subclass_of($controller, 'core\controllers\baseController')) {
      $controller = new $controller;
      return $controller;
    }
    else {
      echo "NO FILE EXISTS";
      return null;
    }

  }
}


?>
