<?php
namespace core\controllers;
/**
*A Controller interface
*Having functions of add update delete and show
*/
interface controllerInterface {

  public function add();
  public function update();
  public function delete();
  public function show();

}

?>
