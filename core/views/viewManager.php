<?php
/**
*View Manager Class
*/
namespace core\views;

use system\classes\request as request;

class viewManager {
  /**
  *A function that takes @param $view and check if the $view exists
  *then include that view that will use in baseController
  */

  public function View($view,$param = null)
  {
    $req = request::getInstance();
    $controller = $req->getController();
    $file = "../app/views/".$controller."/".$view.".php";
    if(file_exists($file)){
      require_once($file);
    }
    else {
      echo "No View Found";
    }
  }

}



?>
