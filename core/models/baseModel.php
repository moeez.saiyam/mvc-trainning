<?php
namespace core\models;
use core\models\modelInterface as modelInterface;
use core\models\database\mysqli\Driver as Driver;
use core\models\database\queryBuilder\queryBuilder as queryBuilder;
use system\classes\request as request;

/**
*Base Model class
*/
class baseModel implements modelInterface {

  private $queryBuild;
  private $sqliDriver;
  private $modelentity;
  private $metadata;


  /**
  *On creating new instance of base model constructor calls and it initialize
  *queryBuilder, SQLDriver, and ModelEntity classes
  */
  public function __construct()
  {
    $this->queryBuild = new queryBuilder();
    $this->sqliDriver = Driver::getInstance();
    $this->metadata = 'app\models\metadata\\' . request::getInstance()->getController();
    if(request::getInstance()->getController() != 'home')
    {
      $this->modelentity = new $this->metadata;
    }

  }
  /**
  *Function add first makes an insert query by taking modelentity,
  *pass that query to sqliDriver and execute that query and if result is TRUE
  *query is successfully executed otherwise error occured
  */
  public function add ()
  {

    $query = $this->queryBuild->queryAdd($this->modelentity);

    $result = $this->sqliDriver->execute($query);

    if($result === TRUE)
    {
      return true;
    }
    else{
      return false;
    }
  }

  /**
  *Function update first makes an update query by taking modelentity,
  *pass that query to sqliDriver and execute that query and if result is TRUE
  *query is successfully executed otherwise error occured
  */
  public function update ()
  {

    $query = $this->queryBuild->queryUpdate($this->modelentity);

    $result = $this->sqliDriver->execute($query);
    if($result === TRUE)
    {
      return true;
    }
    else{
      return false;
    }
  }

  /**
  *Function delete first makes a delete query by taking modelentity,
  *pass that query to sqliDriver and execute that query and if result is TRUE
  *query is successfully executed otherwise error occured
  */
  public function delete ()
  {

    $query = $this->queryBuild->queryDelete($this->modelentity);

    $result = $this->sqliDriver->execute($query);
    if($result === TRUE)
    {
      return true;
    }
    else{
      return false;
    }

  }

  /**
  *Function show first makes a show query by taking modelentity,
  *pass that query to sqliDriver and execute that query and if result is TRUE
  *query is successfully executed otherwise error occured
  */
  public function show ()
  {

    $query = $this->queryBuild->queryShow($this->modelentity);

    $result = $this->sqliDriver->execute($query);

    if($result)
    {
      return $result;
    }
    else{
      return false;
    }
  }

  /**
  *Function Entity is responsible for making a model entity. It will be called by controller
  *in the basecontrollerclass
  */
  public function makeEntity($params)
  {

    if(isset($params['id'])){
      $this->modelentity->id = $params['id'];
      unset($params['id']);
    }
    foreach ($this->modelentity->dbfields as $key) {
      $this->modelentity->{$key} = $params[$key];
    }
  }
  /**
  *Setter Function that sets QueryBuild  @param
  */
  public function setqueryBuilder($queryBuild)
  {
    $this->queryBuild = $queryBuild;
  }
  /**
  *Setter Function that sets SQLIDriver @param
  */
  public function setsqliDriver($sqliDriver)
  {
    $this->sqliDriver = $sqliDriver;
  }
  /**
  *Setter Function that sets model entity @param
  */
  public function setmodelentity($modelentity)
  {
    $this->modelentity = $modelentity;
  }
}

?>
