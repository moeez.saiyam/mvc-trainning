<?php

namespace core\models\database\queryBuilder;
/**
*Class QueryBuilder build a query by taking modelentity
*/
class queryBuilder {

  /**
  *Function queryAdd returns an insert query by taking modelentity as @param
  */
  public function queryAdd($param)
  {
    $query =  "insert into " . $param->tablename . "(";
    foreach ($param->dbfields as $fields)
    {
      $query = $query . $fields . ",";
    }
    $query = rtrim($query, ",");
    $query = $query . ") values (";
    foreach ($param->dbfields as $values)
    {
      $query = $query ."'".  $param->{$values} . "',";
    }
    $query = rtrim($query, ",");
    $query = $query . ")";

    return $query;

  }

  /**
  *Function queryDelete returns a delete query by taking modelentity as @param
  */
  public function queryDelete($param)
  {
    $query = "delete from " . $param->tablename . " where " . $param->dbfields[0] . "= '" . $param->{$param->dbfields[0]} ."'";
    //print_r($query);
    return $query;
  }

  /**
  *Function queryShow returns a show query by taking modelentity as @param
  */
  public function queryShow($param)
  {
    $query = "Select * from ". $param->tablename;
    return $query;
  }

  /**
  *Function queryUpdate returns an update query by taking modelentity as @param
  */
  public function queryUpdate($param)
  {
    $query =  "UPDATE " . $param->tablename . " SET ";
    foreach ($param->dbfields as $fields)
    {
      $query = $query . $fields . "=";
      $query = $query . "'".  $param->{$fields} . "',";
    }
    $query = rtrim($query, ",");
    $query = $query . " where id = " . $param->id;

    return $query;
  }
}


?>
