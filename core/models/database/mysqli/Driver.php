<?php

namespace core\models\database\mysqli;
/**
*SQLI Driver class that interacts with Database. Is responsible for making connection,
*executing query and closing connection
*/
class Driver {

  private $link;
  private static $instance = null;
  private $dbhost = "localhost";
  private $dbuser = "root";
  private $dbpass = "123";
  private $dbname = "mvcDB";

  private function __construct(){

    $this->link = mysqli_connect($this->dbhost, $this->dbuser, $this->dbpass,$this->dbname);

  }
  /**
  *Function getInstance returns instance of this class and makes a new if it is
  *null previously
  */
  public static function getInstance()
  {
    if(!self::$instance)
    {
      self::$instance = new Driver();
    }
    return self::$instance;
  }
  /**
  *The use of __wakeup() as private is to prevent reestablish any connections that
  *may have been lost during serialization and perform other reinitialization tasks.
  */
  private function __wakeup()
  {

  }
  /**
  *This is made private to prevent any cloning of the object
  */
  private function __clone()
  {

  }
  /**
  *It is used as private to prevent to return an array with the names of all
  *variables of that object that should be serialized.
  */
  private function __sleep()
  {

  }

  /**
  *func returns connection of database
  */
  public function getConnection()
  {
    return $this->link;
  }
  /**
  *func Close DB connection
  */
  public function closeConnection()
  {
    $this->link->close();
  }
  /**
  *Execute the query and returns status
  */
  public function execute($sql)
  {
    $conn = $this->getConnection();

    if($conn) {
      $result = $conn->query($sql);

      if(is_object($result)) {

        if($result->num_rows > 0)
        {
          while ($data = $result->fetch_assoc())
          {
            $displaydata[] = $data;
          }
          return $displaydata;
        }
        else{

          return false;
        }
      }
      else {
        if($conn->affected_rows > 0)
        {
          return $result;
          $this->closeConnection();
        }
        else {
          return false;
        }
      }
    }
    else{
      print_r("ERROR: DB CONN");
      return false;
    }

  }
}



?>
