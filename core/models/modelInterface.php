<?php
namespace core\models;
/**
 *Interface of base Model that has add, update, delete and show fumction 
 */
interface modelInterface
{
  public function add();
  public function update();
  public function delete();
  public function show();
}


?>
