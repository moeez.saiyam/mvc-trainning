<?php
namespace core\models;
use core\models\baseModel as baseModel;

/**
*Model Factory Class responsible for making an instance of model if it exists else {
*it return null
*/
class modelFactory {

  public function __construct()
  {
  }

  public static function buildModel ($model)
  {
    $model = $model."Model";
    $model = 'app\models\\'.$model;
    if (is_subclass_of($model, 'core\models\baseModel')) {
      $model = new $model;
      return $model;
    }
    else {

      return null;
    }
  }
}

?>
