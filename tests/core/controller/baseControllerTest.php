<?php


use PHPUnit\Framework\TestCase;


use core\controllers\baseController as baseController;
use app\models\studentModel as studentModel;
use core\views\viewManager as viewManager;
use system\classes\request as request;

class baseControllertest extends TestCase
{
  private $req;
  private $model;
  private $controller;
  private $view;

  protected function setUp()
  {
    $this->req = $this->createMock(request::class);
    $this->model = $this->createMock(studentModel::class);
    $this->view = $this->createMock(viewManager::class);
    $this->controller = new baseController;

  }

  protected function tearDown()
  {

  }

  public function testqueryAdd()
  {

    $params = array('firstname'=>"test",'lastname'=>"test"
     , 'contact'=>"test" , 'address'=>"test");

    $this->req->method('getMethod')
            ->willReturn('add');
    $this->req->method('getController')
            ->willReturn('student');
    $this->req->method('getParams')
            ->willReturn($params);
    $this->model->method('makeEntity')
            ->willReturn(true);
    $this->model->method('add')
            ->willReturn(true);


    $this->controller->setreq($this->req);
    $this->controller->setmodel($this->model);
    $this->controller->setview($this->view);

    $result = $this->controller->add();
    $expectedResult=true;

    $this->assertEquals($result,$expectedResult);

  }


  public function testqueryShow()
  {

    $this->req->method('getMethod')
            ->willReturn('show');
    $this->req->method('getController')
            ->willReturn('student');

    $this->model->method('show')
            ->willReturn(true);

    $this->controller->setreq($this->req);
    $this->controller->setmodel($this->model);
    $this->controller->setview($this->view);

    $result = $this->controller->show();
    $expectedResult=true;

     $this->assertEquals($result,$expectedResult);

  }

  public function testqueryDelete()
  {
    $params = array('firstname'=>"test");

    $this->req->method('getMethod')
            ->willReturn('delete');
    $this->req->method('getController')
            ->willReturn('student');
    $this->req->method('getParams')
            ->willReturn($params);
    $this->model->method('makeEntity')
            ->willReturn(true);
    $this->model->method('delete')
            ->willReturn(true);


    $this->controller->setreq($this->req);
    $this->controller->setmodel($this->model);
    $this->controller->setview($this->view);

    $result = $this->controller->delete();
    $expectedResult=true;

     $this->assertEquals($result,$expectedResult);

  }

  public function testqueryUpdate()
  {
    $params = array('id'=>1 , 'firstname'=>"test",'lastname'=>"test"
     , 'contact'=>"test" , 'address'=>"test");

    $this->req->method('getMethod')
            ->willReturn('update');
    $this->req->method('getController')
            ->willReturn('teacher');
    $this->req->method('getParams')
            ->willReturn($params);
    $this->model->method('makeEntity')
            ->willReturn(true);
    $this->model->method('update')
            ->willReturn(true);


    $this->controller->setreq($this->req);
    $this->controller->setmodel($this->model);
    $this->controller->setview($this->view);

    $result = $this->controller->update();
    $expectedResult=true;

    $this->assertEquals($result,$expectedResult);

  }

  public function testCallAction()
  {

    $this->req->method('getMethod')
            ->willReturn('update');
    $this->req->method('getController')
            ->willReturn('teacher');

    $this->controller->setreq($this->req);
    $this->controller->setmodel($this->model);

    $result = $this->controller->callAction();
    $expectedResult=true;

    $this->assertEquals($result,$expectedResult);

  }


}



?>
