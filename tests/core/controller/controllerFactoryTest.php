<?php

use PHPUnit\Framework\TestCase;


use core\controllers\controllerFactory as controllerFactory;
use app\controllers\studentController as studentController;
use app\controllers\teacherController as teacherController;
use app\controllers\courseController as courseController;
use app\controllers\homeController as homeController;

class controllerFactoryTest extends TestCase
{
  private $controllerStu = "student";
  private $controllerteach = "teacher";
  private $controllerhome = "home";
  private $controllercourse = "course";
  protected function setUp()
  {
    $this->controllerStu = controllerFactory::buildController($this->controllerStu);
    $this->controllerteach = controllerFactory::buildController($this->controllerteach);
    $this->controllerhome = controllerFactory::buildController($this->controllerhome);
    $this->controllercourse = controllerFactory::buildController($this->controllercourse);
  }

  protected function tearDown()
  {
    $this->controllerStu = null;
    $this->controllerteach = null;
    $this->controllerhome = null;
    $this->controllercourse = null;
  }

  public function testStudentControllerBuild()
  {
      $sController = new studentController;
      $this->assertEquals($this->controllerStu, $sController);
  }
  public function testTeacherControllerBuild()
  {
      $tController = new teacherController;
      $this->assertEquals($this->controllerteach, $tController);
  }
  public function testHomeControllerBuild()
  {
      $hController = new homeController;
      $this->assertEquals($this->controllerhome, $hController);
  }
  public function testCourseControllerBuild()
  {
      $cController = new courseController;
      $this->assertEquals($this->controllercourse, $cController);
  }
}

 ?>
