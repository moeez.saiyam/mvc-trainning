<?php

use PHPUnit\Framework\TestCase;


use core\models\database\queryBuilder\queryBuilder as queryBuilder;
use app\models\metadata\student as student;

class queryBuilderTest extends TestCase
{
  private $query;
  private $param;

  protected function setUp()
  {
    $this->query = new queryBuilder();
    $this->param = new student;
    $this->param->id = "1";
    $this->param->firstname = "Moeez";
    $this->param->lastname = "Saiyam";
    $this->param->contact = "0300";
    $this->param->address = "XYZ";
  }

  protected function tearDown()
  {
    $this->query = NULL;
  }

  public function testqueryAdd()
  {
    $result = $this->query->queryAdd($this->param);
    $query = "insert into student(firstname,lastname,contact,address) values ('Moeez','Saiyam','0300','XYZ')";
    $this->assertEquals($query, $result);
  }
  public function testqueryDelete()
  {
    $result = $this->query->queryDelete($this->param);
    $query = "delete from student where firstname= 'Moeez'";
    $this->assertEquals($query, $result);
  }
  public function testqueryUpdate()
  {
    $result = $this->query->queryUpdate($this->param);
    $query = "UPDATE student SET firstname='Moeez',lastname='Saiyam',contact='0300',address='XYZ' where id = 1";

    $this->assertEquals($query, $result);
  }
  public function testqueryShow()
  {
    $result = $this->query->queryShow($this->param);
    $query = "Select * from student";

    $this->assertEquals($query, $result);
  }

}

?>
