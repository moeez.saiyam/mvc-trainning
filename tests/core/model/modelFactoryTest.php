<?php


use PHPUnit\Framework\TestCase;


use core\models\modelFactory as modelFactory;
use app\models\studentModel as studentModel;
use app\models\teacherModel as teacherModel;
use app\models\courseModel as courseModel;

class modelFactoryTest extends TestCase
{
  private $modelStu = "student";
  private $modelTeach = "teacher";
  private $modelCourse = "course";
  protected function setUp()
  {
    $this->modelStu = modelFactory::buildModel($this->modelStu);
    $this->modelTeach = modelFactory::buildModel($this->modelTeach);
    $this->modelCourse = modelFactory::buildModel($this->modelCourse);
  }

  protected function tearDown()
  {
    $this->modelStu = null;
    $this->modelTeach = null;
    $this->modelCourse = null;
  }

  public function testStudentModelBuild()
  {
      $sModel = new studentModel;
      $this->assertEquals($this->modelStu, $sModel);
  }
  public function testTeacherModelBuild()
  {
      $tModel = new teacherModel;
      $this->assertEquals($this->modelTeach, $tModel);
  }
  public function testCourseModelBuild()
  {
      $cModel = new courseModel;
      $this->assertEquals($this->modelCourse, $cModel);
  }
}

 ?>
