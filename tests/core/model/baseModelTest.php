<?php

use PHPUnit\Framework\TestCase;



use core\models\baseModel as baseModel;
use core\models\database\mysqli\Driver as Driver;
use core\models\database\queryBuilder\queryBuilder as queryBuilder;
use app\models\metadata\student as student;
use app\models\metadata\teacher as teacher;
use app\models\metadata\course as course;

class baseModeltest extends TestCase
{

  private $req;
  private $queryBuild;
  private $sqliDriver;
  private $studententity;
  private $teacherentity;
  private $courseentity;
  private $model;


  protected function setUp()
  {
    $this->queryBuild = $this->createMock(queryBuilder::class);
    $this->sqliDriver = $this->createMock(Driver::class);
    $this->studententity = $this->createMock(student::class);
    $this->teacherentity = $this->createMock(teacher::class);
    $this->courseentity = $this->createMock(course::class);
    $this->model = new baseModel;

  }

  protected function tearDown()
  {

  }

  public function testmodelAdd()
  {

    $params = array('firstname'=>"test",'lastname'=>"test"
     , 'contact'=>"test" , 'address'=>"test");
    $query = "insert into student (firsename, lastname, contact, address)
              values ('test','test','test','test')";

    if(isset($params['id'])){
      $this->studententity->id = $params['id'];
      unset($params['id']);
    }

    foreach ($this->studententity->dbfields as $key) {
      $this->studententity->{$key} = $params[$key];
    }

    $this->queryBuild->method('queryAdd')
              ->willReturn($query);
    $this->sqliDriver->method('execute')
              ->willReturn(true);

    $this->model->setqueryBuilder($this->queryBuild);
    $this->model->setsqliDriver($this->sqliDriver);
    $this->model->setmodelentity($this->studententity);
    $result = $this->model->add();
    $expectedResult=true;
     $this->assertEquals($result,$expectedResult);

  }

  public function testmodelDelete()
  {

    $params = array('firstname'=>"test", 'lastname'=>"",'contact'=>"",'address'=>"");
    $query = "delete from student where firstname = 'test'";

    if(isset($params['id'])){
      $this->studententity->id = $params['id'];
      unset($params['id']);
    }

    foreach ($this->studententity->dbfields as $key) {
      $this->studententity->{$key} = $params[$key];
    }

    $this->queryBuild->method('queryDelete')
              ->willReturn($query);
    $this->sqliDriver->method('execute')
              ->willReturn(true);

    $this->model->setqueryBuilder($this->queryBuild);
    $this->model->setsqliDriver($this->sqliDriver);
    $this->model->setmodelentity($this->studententity);

    $result = $this->model->delete();
    $expectedResult=true;

    $this->assertEquals($result,$expectedResult);

  }

  public function testmodelUpdate()
  {
    $params = array('id' => "1",'firstname'=>"test",'lastname'=>"test", 'contact'=>"test" , 'address'=>"test");
    $query = "update teacher set 'firsename' = 'test', 'lastname' = 'test', 'contact' = 'test', 'address'='test' where 'id'='1'";


    if(isset($params['id'])){
      $this->teacherentity->id = $params['id'];
      unset($params['id']);
    }

    foreach ($this->teacherentity->dbfields as $key) {
      $this->teacherentity->{$key} = $params[$key];
    }

    $this->queryBuild->method('queryUpdate')
              ->willReturn($query);
    $this->sqliDriver->method('execute')
              ->willReturn(true);

    $this->model->setqueryBuilder($this->queryBuild);
    $this->model->setsqliDriver($this->sqliDriver);
    $this->model->setmodelentity($this->teacherentity);
    $result = $this->model->update();
    $expectedResult=true;

     $this->assertEquals($result,$expectedResult);

  }

  public function testmodelShow()
  {
    $query = "select * from course";
    $this->queryBuild->method('queryShow')
              ->willReturn($query);
    $this->sqliDriver->method('execute')
              ->willReturn(true);

    $this->model->setqueryBuilder($this->queryBuild);
    $this->model->setsqliDriver($this->sqliDriver);
    $this->model->setmodelentity($this->courseentity);
    $result = $this->model->show();
    $expectedResult=true;

    $this->assertEquals($result,$expectedResult);

  }

}

?>
