<?php

/*
* Autoloader defined by namespaces
*/

spl_autoload_register(function ($class) {

 $root = dirname(__FILE__);
 //echo $root;
 $file = $root . '/' . str_replace('\\', '/', $class) . '.php';
 //echo "<br>".$file."<br>";
 if (file_exists($file))
 {
   require_once $root . '/' . str_replace('\\', '/', $class) . '.php';
 }
});


?>
