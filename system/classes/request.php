<?php
namespace system\classes;

/*
*A Singleton CLass of request that has @param $controller $parameter and $method
*/

class request {
  /**
  *default controller
  *Deafult method
  *Default Parameter
  */

  private static $instance = null;

  private $controller = 'home';
  private $method = 'show';
  private $params = [];

  /**
  *In default constructir we check if super global @param  $request is not emptry
  *it get controller, method and parameters from that request and if $request is
  *empty it assigns default value in controller and action that redirect to homepage
  */
  private function __construct()
  {

    //include_once ('../core/controllers/controllerFactory.php');
    if(!empty($_REQUEST))
    {
      $this->controller = $_REQUEST["controller"];
      $this->method = $_REQUEST['action'];
      unset($_REQUEST['controller']);
      unset($_REQUEST['action']);
      $this->params = $_REQUEST;
    }
    else
    {
      $this->controller = "home";
      $this->method = "show";
    }
  }
  /**
  *The use of __wakeup() as private is to prevent reestablish any connections that
  *may have been lost during serialization and perform other reinitialization tasks.
  */
  private function __wakeup()
  {
  }
  /**
  *This is made private to prevent any cloning of the object
  */
  private function __clone()
  {
  }
  /**
  *It is used as private to prevent to return an array with the names of all
  *variables of that object that should be serialized.
  */
  private function __sleep()
  {
  }

  /**
  *Function getInstance returns instance of this class and makes a new if it is
  *null previously
  */
  public static function getInstance()
  {
    if (self::$instance == null)
    {
      self::$instance = new request ();
    }
    return self :: $instance;
  }

  /**
  *Returns @param $controller
  */
  public function getController(){
    return $this->controller;
  }
  /**
  *Returns @param $method
  */
  public function getMethod() {

    return $this->method;
  }
  /**
  *Returns @param $params
  */
  public function getParams() {
    return $this->params;
  }


}

?>
