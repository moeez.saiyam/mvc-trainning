<?php

namespace system\init;

use system\classes\request as request;
use core\controllers\controllerFactory as controllerFactory;

/**
*Initiate request by making instance of request/Request
*gets controller method and parameter of url
*call Controller Factory and build new controller
*/
class initReq {
  //$req = new request;
  public function __construct()
  {

    $req = request::getInstance();

    $controller = controllerFactory::buildController($req->getController());

    $controller->callAction();
  }

}

?>
